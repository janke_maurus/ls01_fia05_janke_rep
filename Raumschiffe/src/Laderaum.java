import java.util.ArrayList;

public class Laderaum {
	public ArrayList<ArrayList<Object>> Ladung;
	
	public ArrayList<ArrayList<Object>> getLadung(){
		return Ladung;
	}
	public void setLadung(ArrayList<ArrayList<Object>> ladung) {
		Ladung = ladung;
	}
	public void addLadung(String name, int anzahl) {
		if (Ladung == null) {
			this.setLadung(new ArrayList<>());
		}
		ArrayList<Object> ladungsAbteil=new ArrayList<Object>();
		ladungsAbteil.add(name);
		ladungsAbteil.add(anzahl);
		this.Ladung.add(ladungsAbteil);
	}
	
	public void ladungAufraeumen() {
		if (Ladung!=null) {
			int n=0;
			for (ArrayList<Object> ladungsAbteil : this.Ladung) {
				if ((int) ladungsAbteil.get(1)<=0) {
					this.Ladung.remove(n);
					System.out.println(ladungsAbteil.get(0)+"wurde aufgeräumt!");
				}
				n++;
			}
		}
		else 
			System.out.println("Keine Ladung gefunden!");
	}
	public void ladungAufzaehlen() {
		if (Ladung!=null) {
			int n=0;
			for (ArrayList<Object> ladungsAbteil : this.Ladung) {
				n++;
				System.out.println(n+ ".Ladung: " + ladungsAbteil.get(0) + "     Menge: " + ladungsAbteil.get(1));
			}
		}
		else
			System.out.println("Keine Ladung gefunden!");
	}	
}
	