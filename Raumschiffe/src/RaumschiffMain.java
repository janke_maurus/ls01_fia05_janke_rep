/**
 * @author Maurus Janke
 */

public class RaumschiffMain {

	public static void main(String[] args) {
		Raumschiff r1 = new Raumschiff();
		r1.setName("Borg");
		
		Raumschiff r2 = new Raumschiff();
		r2.setName("Enterprise");
		
		
		r1.setEnergieversorgung(100);
		r1.setHuelle(100);
		r1.setSchilde(100);
		r1.setLebenserhalung(100);
		r1.setAndroiden(30);
		r1.setPhotonentorpedo(10);
		
		r2.setEnergieversorgung(100);
		r2.setHuelle(1);
		r2.setSchilde(100);
		r2.setLebenserhalung(100);
		r2.setAndroiden(30);
		r2.setPhotonentorpedo(10);
		
		/*
		r2.torpedosladen();
		r2.torpedosladen();
		r1.torpedosladen();
		r1.zustand();
		r2.zustand();
		*/
		for (int i=0; i<10; i++)
			r2.reparien();
		r2.zustand();
		r1.nachricht("test Nachricht");
		
		r2.phaserAbschießen();
		r1.treffer();
		r1.zustand();
	}

}
