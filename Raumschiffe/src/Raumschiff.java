import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	//Attribute
		private String name;
		private String nachricht;
		private String ladung;
		private double energiev;				//in Prozent
		private double schilde;					//in Prozent
		private double leber;					//in Prozent
		private double huelle;					//in Prozent
		private int torpedo;
		private int torpedoLader;				//gelandene Torpedos
		private int android;
		private int[] Kommunikator;
		ArrayList<Object> lad1 = new ArrayList<Object>();
		
		//Construktor
		public Raumschiff() {};
		
	//Methoden
		
		/**
		 * @return Getter und Setter der Photonentopedos
		 * @param torpedo
		 */
		public void setPhotonentorpedo (int torpedo) {
			this.torpedo = torpedo;
		}
		public int getPhotonentorpedo () {
			return this.torpedo;
		}
		/**
		 * @return Getter und Setter der Engergieversorgung
		 * @param engergiev
		 */
		public void setEnergieversorgung (double energiev) {
			this.energiev = energiev;
		}
		public double getEnergieversorgung () {
			return this.energiev;
		}
		/**
		 * @return Getter und Setter der Schilde
		 * @param schilde
		 */
		public void setSchilde (double schilde) {
			this.schilde = schilde;
		}
		public double getSchilde () {
			return this.schilde;
		}
		/**
		 * @return Getter und Setter der Hülle
		 * @param huelle
		 */
		public void setHuelle (double huelle) {
			this.huelle = huelle;
		}
		public double getHuelle () {
			return this.huelle;
		}
		/**
		 * @return Getter und Setter der Lebenserhaltungssysteme
		 * @param leber
		 */
		public void setLebenserhalung (double leber) {
			this.leber = leber;
		}
		public double getLebenserhalung () {
			return this.leber;
		}
		/**
		 * @return Getter und Setter der Androiden
		 * @param android
		 */
		public void setAndroiden (int android) {
			this.android = android;
		}
		public int getAndroiden () {
			return this.android;
		}
		/**
		 * @return Getter und Setter des Schiffsnamen
		 * @param name
		 */
		public void setName(String name) {
			this.name = name;
		}
		public String getName() {
			return this.name;
		}
		/**
		 * @return Methode zum Torpedo abschießen
		 * @param torpedolader
		 */
		public void torpedoschiessen (int torpedolader) {
			if(this.torpedoLader == 0)
				System.out.println("-=*Click*=-");
			else
			this.torpedoLader -= 1;
			treffer();
		}
		/**
		 * @return Methode zur verarbeitung der Treffer
		 * @param schilde
		 * @param huelle
		 */
		public void treffer() {
			System.out.println("Treffer!");
			if (this.schilde > 0)
				this.schilde -= 10;
			else if (this.schilde < 10) {
				double i = 10 - this.schilde;
				this.schilde = 0;
				this.huelle -= i;
				System.out.println("Die Schilde vom Raumschiff "+this.name+"sind zusammengebrochen!");
			}
			else if(this.schilde == 0 || this.huelle > 10) {
				this.huelle -= 10;
			}
			else if(this.huelle < 10) {
				this.setHuelle(0);
				this.setLebenserhalung(0);
				this.setEnergieversorgung(0);
				System.out.println("Das Raumschiff "+this.name+" wurde zerstört!");
			}
		}
		/**
		 * @return Methode um eine Globale nachricht zu sehen
		 * @param nachricht
		 */
		public void nachricht(String nachricht) {
			this.nachricht = nachricht;
			System.out.printf("%s%n%s%n%s%n%s%n","----------------------------------------","An alle:",nachricht,"----------------------------------------");
		}
		/**
		 * @return Methode um die Phaser zu benutzen
		 * @param energiev
		 */
		public void phaserAbschießen () {
			if (this.energiev<50)
				System.out.println("*Nicht genug Energie*");
			else {
				setEnergieversorgung(this.energiev-50);
				System.out.println("Phaserkanone abgeschossen");
				treffer();
				System.out.println("Die Energie versorgung ist bei " + this.energiev +"%!");
			}
		}
		/**
		 * @return Methode um die Torpedus zu laden um sie anschließend feuern zu können
		 * @param torpedo
		 * @param torpedoLader
		 */
		public void torpedosladen () {
			if (this.torpedoLader > 4)
				System.out.println("Alle Rohre sind geladen und feuerbereit!");
			else {
				this.torpedoLader += 1;
				this.torpedo -= 1;
			}
		}
		/**
		 * @return Methode zur Reparation der Hülle
		 * @param huelle
		 * @param android
		 */
		public void reparien () {
			//Zufall für Androidschaden
			Random zf = new Random();
			int zahl;			
			zahl = 1 + zf.nextInt(100);
			
			if (this.huelle > 99)
				System.out.println("Deine Hülle ist bereits vollständig repariert");
			else if ( this.huelle > 90) {
				double rep = 100 - this.huelle;
				this.huelle += rep;
				if (zahl == 65)
					this.android -= 1;
			}
			else {
				this.huelle = this.huelle + 10;
				if (zahl > 70)
					this.android -= 1;
			}
		}
		/**
		 * @return Methode um den Status der Raumschiffe auszugeben
		 * @param name
		 * @param huelle
		 * @param energiev
		 * @param leber
		 * @param schilde
		 * @param android
		 * @param torpedo
		 * @param torpedoLader
		 */
		public void zustand() {
			System.out.println("***********************************");
			System.out.println("Der Raumschiffzustand:");
			System.out.println("Name: "+ this.name);
			System.out.println("Hüllenintegrität: " + this.huelle + "%");
			System.out.println("Energieversorgung: " + this.energiev + "%");
			System.out.println("Lebenserhalungssysteme: "+ this.leber + "%");
			System.out.println("Schildkapazität: " + this.schilde  + "%");
			System.out.println("-----------------------------------");
			System.out.println("Androiden: "+ this.android);
			System.out.println("Torpedos: " + this.torpedo + " ("+this.torpedoLader+")");
			System.out.println("***********************************");
		}
		//Logbuch
		
}
