import java.util.Scanner;
class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	int frage;
    	do {
    	  double zuZahlenderBetrag = 0.0;       
    	  double eingezahlterGesamtbetrag = 0.0;
    	  frage = 0;
           
    	  zuZahlenderBetrag = fahrkartenbestellungErfassen(zuZahlenderBetrag);
    	  eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	  fahrkartenAusgeben();
    	  rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
    	  frage = weitereKarten(frage);
      }while(frage != 0);
       
    }       
    public static double fahrkartenbestellungErfassen(double zuZahlenderBetrag)
    {
    	Scanner tastatur = new Scanner(System.in);	
    double anzahlDerKarten = 0.0;
    
    //   System.out.print("Zu zahlender Betrag (EURO): ");
    //   zuZahlenderBetrag = tastatur.nextDouble();
    
    System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:");
    System.out.printf("%5s%-20s%n", " ", "Einzelfahrscheine (1)");
    System.out.printf("%5s%-20s%n"," ","Tageskarten (2)");
    System.out.printf("%5s%-20s%n%n%n"," ","Gruppenkarten (3)");
    
    System.out.print("Ihre Wahl: ");
    int Auswahl = tastatur.nextInt();
    
    switch(Auswahl)
    {
    case 1 :
    	zuZahlenderBetrag = 2.90;
    break;
    case 2 :
    	zuZahlenderBetrag = 8.60;
    break;
    case 3 :
    	zuZahlenderBetrag = 15.90;
    break;
    default:
    	System.out.println("Fehler!");
    	break;
    }
    
    System.out.print("Anzahl der Fahrkarten: ");
    anzahlDerKarten = tastatur.nextDouble();
    
    zuZahlenderBetrag = zuZahlenderBetrag * anzahlDerKarten;
    
    return zuZahlenderBetrag;
    }
     
    public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    {       
    Scanner tastatur = new Scanner(System.in);
       double wert = 0.00;
       double eingeworfeneMünze = 0.0;
       while(wert < zuZahlenderBetrag)
       {
    	   System.out.print("Noch zu zahlen: ");
    	   System.out.printf("%.2f%n", (zuZahlenderBetrag - wert));
    	   
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           wert += eingeworfeneMünze;
       }
       return wert;
    }
    
    public static void fahrkartenAusgeben()
    {
        // Fahrscheinausgabe
        // -----------------
        	System.out.println("\nFahrschein wird ausgegeben");
        	for (int i = 0; i < 8; i++)
        	{
        		System.out.print("=");
        		try {
        			Thread.sleep(250);
        		} catch (InterruptedException e) {
        			e.printStackTrace();
        		}
        	}    
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbertag, double zuZahlenderBetrag)
    {
    	
    	//double rückgabebetrag = 0.0;
        double rückgabebetrag = (eingezahlterGesamtbertag - zuZahlenderBetrag);
        if(rückgabebetrag > 0.00)
        {
     	   System.out.printf("%nDer Rückgabebetrag in Höhe von ");
     	   System.out.printf("%.2f", rückgabebetrag);
     	   System.out.println(" Euro");    	       	   
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
            while(rückgabebetrag > 1.99) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.00;
            }
            while(rückgabebetrag > 0.99) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.00;
            }
            while(rückgabebetrag > 0.49) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag > 0.19) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag > 0.09) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag > 0.04)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }

    public static int weitereKarten(int frage)
    {
    	Scanner scan = new Scanner(System.in);
    	char atw = 'n';
    	System.out.printf("%n%nWollen Sie weitere Fahrkarten? (y/n)   ");
    	atw = scan.next().charAt(0);
    	switch(atw)
    	{
    	case 'n':
    		frage = 0;
    	break;
    	case 'y':
    		frage = 1;
    	break;
    	default:
    		frage = 0;
    	break;
    	}
    	//System.out.println(frage);
    	System.out.printf("%n%n%n%n");
    	return frage;
    }
}
