import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class ReadBookstoreData1 {

  public static void main(String[] args) {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    try {

      DocumentBuilder dbuilder = factory.newDocumentBuilder();
      Document doc = dbuilder.parse("buchhandlung.xml");

      Node titel = doc.getElementsByTagName("titel").item(0);
      System.out.println(titel.getNodeName()+": "+titel.getTextContent());
      
      
      
      Node autor = doc.getElementsByTagName("autor").item(0);
      System.out.println(autor.getNodeName()+"; "+autor.getTextContent());

    
    
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}