﻿import java.util.Scanner;
class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	int frage;
    	do {
    	  double zuZahlenderBetrag = 0.0;       
    	  double eingezahlterGesamtbetrag = 0.0;
    	  frage = 0;
           
    	  zuZahlenderBetrag = fahrkartenbestellungErfassen(zuZahlenderBetrag);
    	  eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	  if (zuZahlenderBetrag <= 0.0) {
    	  fahrkartenAusgeben();
    	  }
    	  rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
    	  frage = weitereKarten(frage);
      }while(frage != 0);
       
    }       
    public static double fahrkartenbestellungErfassen(double zuZahlenderBetrag)
    {
    	Scanner tastatur = new Scanner(System.in);	
    double anzahlDerKarten = 0.0;
    
    String[] kartennamen= {"Enzeilfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    double[] preisliste= {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    System.out.printf("%-10s|%-35s|%-10s\n\n","Nummer","Bezeichnung","Preis"); 
    int Auswahl;
    int l = preisliste.length;
    do {
    	for(int i=0;i<l;i++) {
    		String stemp= kartennamen[i];
    		double dtemp=preisliste[i];
    		int rechnung = i+1;
    		String nummer = ""+rechnung;
    		System.out.printf("%-10s|%-35s|%-4.2f€\n",nummer,stemp,dtemp);
    	}
    System.out.print("\nIhre Wahl: ");
    Auswahl = tastatur.nextInt()-1;
    }while(Auswahl >= l);
    zuZahlenderBetrag = preisliste[Auswahl];    
    
    anzahlDerKarten = 1;
    do {
    	do {
    		System.out.print("\nWie viele Katen wollen sie haben? ");
    		anzahlDerKarten = tastatur.nextDouble();
    	} while (anzahlDerKarten >= 11);
    } while (anzahlDerKarten <= 0);
    
    zuZahlenderBetrag = zuZahlenderBetrag * anzahlDerKarten;
    
    return zuZahlenderBetrag;
    }
     
    public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    {       
    Scanner tastatur = new Scanner(System.in);
       double wert = 0.00;
       double eingeworfeneMünze = 0.0;
       while(wert < zuZahlenderBetrag)
       {
    	   System.out.print("Noch zu zahlen: ");
    	   System.out.printf("%.2f%n", (zuZahlenderBetrag - wert));
    	   
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           wert += eingeworfeneMünze;
       }
       return wert;
    }
    
    public static void fahrkartenAusgeben()
    {
        // Fahrscheinausgabe
        // -----------------
        	System.out.println("\nFahrschein wird ausgegeben");
        	for (int i = 0; i < 8; i++)
        	{
        		System.out.print("=");
        		try {
        			Thread.sleep(250);
        		} catch (InterruptedException e) {
        			e.printStackTrace();
        		}
        	}    
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbertag, double zuZahlenderBetrag)
    {
    	
        double rückgabebetrag = (eingezahlterGesamtbertag - zuZahlenderBetrag);
        if(rückgabebetrag > 0.00)
        {
     	   System.out.printf("%nDer Rückgabebetrag in Höhe von ");
     	   System.out.printf("%.2f", rückgabebetrag);
     	   System.out.println(" Euro");    	       	   
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
            while(rückgabebetrag > 1.99) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.00;
            }
            while(rückgabebetrag > 0.99) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.00;
            }
            while(rückgabebetrag > 0.49) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag > 0.19) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag > 0.09) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag > 0.04)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }

    public static int weitereKarten(int frage)
    {
    	Scanner scan = new Scanner(System.in);
    	char atw = 'n';
    	System.out.printf("%n%nWollen Sie weitere Fahrkarten? (y/n)   ");
    	atw = scan.next().charAt(0);
    	switch(atw)
    	{
    	case 'n':
    		frage = 0;
    	break;
    	case 'y':
    		frage = 1;
    	break;
    	default:
    		frage = 0;
    		atw = 'n';
    	break;
    	}
    	System.out.println("Ihre Auswahl: " + atw);
    	System.out.printf("%n%n%n%n");
    	return frage;
    }
}
