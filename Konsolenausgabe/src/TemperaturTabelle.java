
public class TemperaturTabelle {

	public static void main(String[] args) {
		
		//Start der deklarierung und initaliesierung
		
		double c1,c2,c3,c4,c5,f1,f2,f3,f4,f5;
		
		f1 = -20.00;
		f2 = -10.00;
		f3 = 0.00;
		f4 = 20.00;
		f5 = 30.00;
		
		 // Start von der Berechnung
		
		c1 = (f1 - 32) / 1.80;
		c2 = (f2 - 32) / 1.80;
		c3 = (f3 - 32) / 1.80;
		c4 = (f4 - 32) / 1.80;
		c5 = (f5 - 32) / 1.80;
				
		// Start der Ausgabe
		
		System.out.println("Fahrenheit |   Celsius");
		System.out.println("-----------------------");
		System.out.printf("%-11s|%10.6s%n",f1,c1);
		System.out.printf("%-11s|%10.6s%n",f2,c2);
		System.out.printf("%-11s|%10.6s%n","+"+f3,c3);
		System.out.printf("%-11s|%10.5s%n","+"+f4,c4);
		System.out.printf("%-11s|%10.5s%n","+"+f5,c5);
		
	}

}
