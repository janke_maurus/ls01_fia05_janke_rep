import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
 
public class csveinlesen {

	public static void main(String[] args) {
		
		//Aufgabe 1
		System.out.println("### Aufgabe 1 ###");
		try {
			BufferedReader reader = new BufferedReader(new FileReader("studentNameCSV#A1.csv"));
			String zeile = "";
			while((zeile = reader.readLine()) != null) {
				System.out.println(zeile);
				
			}
			reader.close();
			} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		//Platzhalter
		System.out.printf("%n%n%n%n");
		
		//Aufgabe 2
		System.out.println("### Aufgabe 2 ###");
		try {
			BufferedReader reader = new BufferedReader(new FileReader("studentNameCSV#A2.csv"));
			String zeile = "";
			while((zeile = reader.readLine()) != null) {
				//System.out.println(zeile);
				String[] ausgabe =zeile.split(";");
				String temp = ausgabe[1] + " " + ausgabe[0];
				System.out.printf("%-20s%-10s%-10s%-10s%n",temp,ausgabe[2],ausgabe[3],ausgabe[4]);
				
			}
			reader.close();
			} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		//Platzhalter
				System.out.printf("%n%n%n%n");
				
				
				
				
	}
}