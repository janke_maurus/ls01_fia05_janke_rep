	//Methoden + Atribute für Aufgabe3
	public class Schueler {
	
		//Attribute
		private String name;
		private String vorname;
		private String joker;
		private String blamiert;
		private String fragen;
		
		//Construktor
		public Schueler() {};
		
		//Methoden
		//Getter + Setter Name
		public void setName(String name){
			this.name = name;
		}
		public String getName() {
			return this.name;
		}
		//Getter + Setter Vorname
				public void setVorname(String vorname){
					this.vorname = vorname;
				}
				public String getVorname() {
					return this.vorname;
		}
		//Getter + Setter Joker
		public void setJoker(String ausgabe){
			this.joker = ausgabe;
		}
		public String getJoker() {
			return this.joker;
		}
		//Getter + Setter Blamiert
				public void setBlamiert(String blamiert){
					this.blamiert = blamiert;
				}
				public String getBlamiert() {
					return this.blamiert;
		}
		//Getter + Setter Fragen
		public void setFragen(String fragen) {
		this.fragen = fragen;
		}
		public String getFragen() {
			return this.fragen;
		}
		//Ausgabe Schülerdaten
		public void getSchuelerdaten() {
			System.out.println("***********************************");
			System.out.println("Schülerdatenausgabe:");
			System.out.println("Name: " + this.name);
			System.out.println("Vorname: " + this.vorname);
			System.out.println("");
			System.out.println("Joker: " + this.joker);
			System.out.println("Blamiert: " + this.blamiert);
			System.out.println("Fragen: " + this.fragen);
		}
				
	}