public class Wechsel {
	public static void main(String[]args) {
		double[] zahlen = {};
		
		if(zahlen.length==0) {
			zahlen = new double[]{0};
		}
		
		String backup=""+zahlen[0];
		for(int i = 1;i<zahlen.length; i++) {
			backup += ", "+zahlen[i];
		}
		zahlen = drehen(zahlen);
		String ausgabe=ausgabe(zahlen);
		System.out.println(backup);
		System.out.print(ausgabe);
	}
	public static double[] drehen(double[] array) {
		int l = array.length;
		int mitt = l/2;
		if (l%2 ==1) {
			mitt +=0.5;
		}
		for (int i=0; i<mitt; i++) {
			double speicher = array[i];
			array[i] = array[array.length -1-i];
			array[array.length-1-i]=speicher;
		}
		return array;
	}
	public static String ausgabe(double[] array) {
		String ausgabe="";
		ausgabe += array[0];
		for (int i=1; i<array.length; i++) {
			ausgabe += ", "+array[i]; 
		}
		return ausgabe;
	}
}
