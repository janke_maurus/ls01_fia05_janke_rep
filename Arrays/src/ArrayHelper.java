public class ArrayHelper {

	public static void main (String[]args) {
		int[] zahl = {0,1,2,3,4,5,6,7,8,9};
		String konv = konvertieren(zahl);
		System.out.println(konv);
	}
	
	public static String konvertieren(int[] array) {
		if(array.length==0) {
			array = new int[]{0};
		}
		
		String ausgabe = ""+array[0];
		for(int i = 1; i < array.length; i++){
			ausgabe += ", " + array[i];
		}
		
		
		return ausgabe;
	}
}
